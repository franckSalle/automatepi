# coding: utf-8

import pifacedigitalio
from config import *
from threading import Thread
import time
import sys
from datetime import datetime
import json
from io import StringIO
import socket

"""------------------------------------------------------------------------------------"""
""" Reset interface's Piface """
pifacedigitalio.init()
pfd = pifacedigitalio.PiFaceDigital()


"""------------------------------------------------------------------------------------"""
""" Client network socket for <data> exchange """
data = DATA.copy()
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def connectClient():
    """ This connects <client>
    """
    global client
    try:
        client.connect((IP, PORT))
        data['echange'] = True
        print('Client network socket for data exchange connected')
    except socket.error:
        print ("Network socket is down")

def updateData():
    """ This updates <data> with piface's state and sends with <client>
        <data> is serializing with a json object before to send
    """
    global data, pfd, client
    data['i0'] = pfd.input_pins[0].value
    data['i1'] = pfd.input_pins[1].value
    data['i2'] = pfd.input_pins[2].value
    data['i3'] = pfd.input_pins[3].value
    data['i4'] = pfd.input_pins[4].value
    data['i5'] = pfd.input_pins[5].value
    data['i6'] = pfd.input_pins[6].value
    data['i7'] = pfd.input_pins[7].value  
    data['o0'] = pfd.output_pins[0].value
    data['o1'] = pfd.output_pins[1].value
    data['o2'] = pfd.output_pins[2].value
    data['o3'] = pfd.output_pins[3].value
    data['o4'] = pfd.output_pins[4].value
    data['o5'] = pfd.output_pins[5].value
    data['o6'] = pfd.output_pins[6].value
    data['o7'] = pfd.output_pins[7].value
    message = json.dumps(data)
    client.send(message.encode())

def getInputs():
    """ This returns a list that contains the states of inputs
    """
    global pfd
    inputs = list()
    for i in range(0,8):
        inputs.append(pfd.input_pins[i].value) 
    return inputs

"""------------------------------------------------------------------------------------"""    
def init():
    """ This intialises
    """    
    global data, pfd
    print('Function init running')
    data['compteur'] = 0
    """ LED 6 is on """
    pfd.output_pins[6].value = 1
    pfd.output_pins[4].value = 0
    pfd.output_pins[7].value = 0
    pfd.output_pins[3].value = 1
    pfd.output_pins[0].value = 0 
    pfd.output_pins[1].value = 0      
    updateData()
    while not(getInputs() == [1,0, 1, 0, 0, 0, 0, 1]):
        time.sleep(1)
    pfd.output_pins[6].value = 0
    pfd.output_pins[3].value = 0
    updateData()

"""------------------------------------------------------------------------------------""" 
class Action(Thread):
    """ Heritage of the Thread class
    """
    def __init__(self):
        Thread.__init__(self)
        self.suivant = True

    def run(self):
        """ This runs the cycle until the electronic meter is reached
        """
        global data, pfd
        print('Functiun cycle running')
        """ LED 5 is on """
        pfd.output_pins[5].value = 1
        data['compteur'] = data['compteur'] + 1
        updateData()
        while self.suivant and not(pfd.input_pins[4].value == 1):
            """ step 1 : V 1 go to OUT """
            while self.suivant and not(pfd.input_pins[1].value == 1):
                pfd.output_pins[0].value = 1
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[0])
            """ step 2 : V 2 go to OUT """
            while self.suivant and not(pfd.input_pins[3].value == 1):
                pfd.output_pins[1].value = 1
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[1])            
            """ step 3 : V 1 go to IN """
            while self.suivant and not(pfd.input_pins[0].value == 1):
                pfd.output_pins[0].value = 0
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[2])
            """ step 4 : V 1 go to OUT """
            while self.suivant and not(pfd.input_pins[1].value == 1):
                pfd.output_pins[0].value = 1
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[3])            
            """ step 5 : V 2 go to IN """
            while self.suivant and not(pfd.input_pins[2].value == 1):
                pfd.output_pins[1].value = 0
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[4])
            """ step 6 : V 1 go to IN """
            while self.suivant and not(pfd.input_pins[0].value == 1):
                pfd.output_pins[0].value = 0
                updateData()
            updateData()
            if self.suivant : time.sleep(TEMPO[5])
            """ The electronic meter decrements"""
            if self.suivant : 
                pfd.output_pins[2].value = 1  
                time.sleep(1)
                pfd.output_pins[2].value = 0
                data['compteur'] = data['compteur'] + 1
                updateData()
        """The cycle is finished """
        """ LED 4 is on """
        if self.suivant : 
            pfd.output_pins[4].value = 1
            updateData()

    def stop(self):
        """ Function to stop the thread
        """
        self.suivant = False 


"""------------------------------------------------------------------------------------""" 
if __name__ == '__main__':

    """ The console redirects to automate.log """
    log = open('/prog/log/automate.log', 'a')
    sys.stdout = log
    sys.stderr = log
    
    print(str(datetime.now()))
    connectClient()
    updateData()

    while True:

        print('Waiting for the function init')
        while not((pfd.input_pins[5].value == 1) and (pfd.input_pins[6].value == 0) and (pfd.input_pins[7].value == 1)):
            """ LED 6 flashes """
            pfd.output_pins[6].value = 1
            time.sleep(1)
            pfd.output_pins[6].value = 0
            time.sleep(1)
        init()

        print('Waiting for the function action')
        while not((pfd.input_pins[5].value == 1) and (pfd.input_pins[6].value == 1) and (pfd.input_pins[7].value == 1) and (pfd.input_pins[4].value == 0)):
            """ LED 5 flashes """
            pfd.output_pins[5].value = 1
            time.sleep(1)
            pfd.output_pins[5].value = 0
            time.sleep(1)
        action = Action()
        action.start()

        suivant = True
        while suivant:
            if (pfd.input_pins[7].value == 0):
                print('Emergency stop')
                action.stop()
                """ LED 7 is on """
                pfd.output_pins[7].value = 1
                suivant = False
            if (pfd.input_pins[6].value == 0):
                print('Cycle stop')
                action.stop()
                suivant = False
            if (pfd.output_pins[4].value == 1):
                print('Cycle finished')
                suivant = False
        """ LED 5 is off """
        pfd.output_pins[5].value = 0
        updateData()
    
  
        
