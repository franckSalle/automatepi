# interface.py
# coding:utf-8

from flask import Flask, render_template, redirect, url_for, request, session
from config import *
from threading import RLock, Thread
from datetime import datetime
import os
import re
import json
from io import StringIO
import socket


verrouData = RLock()
data = DATA.copy()

""" Server network socket for <data> exchange """
serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def connectServeur():
    """ This connects <client>
    """
    global serveur, client
    try:
        serveur.bind((IP, PORT))
        print ("Server network socket for data exchange connected, waiting for client")
        serveur.listen(1)
        client, ipClient = serveur.accept()
        print ("Client connected")
    except socket.error:
        print ("Network socket is down")

def updateData():
    """ This receives and updates <data>
    """
    global client, data, verrouData
    while True:     
        message = client.recv(1024)
        with verrouData:
            data = json.loads(message.decode())

""" Web server flask """
app = Flask(__name__)
""" To get key for sessions's variables """
app.config['SECRET_KEY'] = os.urandom(24)

@app.route('/', methods=['GET', 'POST'])
def login():
    if (request.method == 'GET'):
        return render_template('login.html', ip=IP)                    
    if request.method == 'POST':
        """ To get the sended user """
        user = request.form['user']
        """ To clean the string """
        user = re.sub(r'\W', '', user)
        if (user in PASS):
            """ The user is known"""
            session['user'] = user
            session.modified = True
            return redirect(url_for('accueil'))
        return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session['user'] = None
    session.modified = True    
    return redirect(url_for('login'))

@app.route('/accueil', methods=['GET', 'POST'])
def accueil():
    global data
    """ To control access """
    if (session['user'] in PASS):
        if (request.method == 'GET'):
            with verrouData:
                return render_template('accueil.html', ip=IP, data=data)
    else:
        return render_template('login.html', ip=IP)

""" Programm """
if __name__ == '__main__':

    connectServeur()
    
    """ To run with parallel execution the function updateData """ 
    uds = Thread(target=updateData)
    uds.start()

    app.run(host='0.0.0.0', debug=False)
    
