# config.py
# coding: utf-8

PASS = ['password']
IP = '###.###.###.###'
PORT = 40000
TEMPO = (5,5,5,5,5,5)

""" This dict describes piface's states
    "echange": network's state
    "i0": 0, # switch on if hydraulic cylinder V1 is IN position
    "i1": 0, # switch on if hydraulic cylinder V1 is OUT position
    "i2": 0, # switch on if hydraulic cylinder V2 is IN position
    "i3": 0, # switch on if hydraulic cylinder V2 is OUT position
    "i4": 0, # goal of the electronic meter reached
    "i5": 0, # pushbutton to start the cycle
    "i6": 0, # switch On/Off cycle
    "i7": 0, # pushbutton emergency stop, mode NC
    "o0": 0, # to turn on V1
    "o1": 0, # to turn on V2
    "o2": 0, # to decrement the electronic meter
    "o3": 0, # to reset the electronic meter
    "o4": 0, # indicator light successful
    "o5": 0, # indicator light automate running
    "o6": 0, # indicator light automate initializing
    "o7": 0,  # indicator light automate emergency stopping
    "compteur" : 0 # number cycle
"""
DATA = {
    "echange": False,
    "i0": 0, "i1": 0, "i2": 0, "i3": 0, "i4": 0, "i5": 0, "i6": 0, "i7": 0,
    "o0": 0, "o1": 0, "o2": 0, "o3": 0, "o4": 0, "o5": 0, "o6": 0, "o7": 0,
    "compteur" : 0
    } 
