﻿# Objectif

On va tenter de remplacer un automate Siemens hors d’usage assurant l’action de deux vérins hydrauliques pendant un nombre de cycles demandé, avec le trio Raspberry Pi, la carte Piface et du code en Python.
Matériel et logiciel utilisés.  

    Raspberry Pi 3B+ rev 1.3
    Carte Piface Digital 2.
    Python3
    Pifacedigitalio
    Flask
    Pifacecommon

# Installation

On installe Raspbian Lite via Noobs.  

https://www.raspberrypi.org/app/uploads/2012/04/quick-start-guide-v2.pdf  

On s’identifie avec le couple login/password par défaut pi/raspberry.  

On active le compte root puis on se logue en root pour la suite.  

    sudo passwd root
    su

On accède au menu de configuration pour modifier le mot de passe, le nom de la machine, activer l’interface SPI utilisée par la carte Piface2 et activer le serveur SSH.  

    raspi-config

On met à jour la distribution.  

    apt-get update && apt-get upgrade -y

On installe l’outil de gestion des paquets pour Python3.  

    apt-get install python3-pip

On installe les dernières versions des paquets Python3 relatifs à la carte Piface Digital 2.  

    pip3 install pifacecommon pifacedigitalio

Puis les docs en rapport.  

    apt-get install python3-pifacedigitalio -y

On teste avec un exemple Python fourni : la simulation d’une entrée par son interrupteur provoque l’activation de la LED de la sortie correspondante.  

    python3 /usr/share/doc/python3-pifacedigitalio/examples/presslights.py﻿

On attribue une IP fixe à la carte wifi en ajoutant au fichier /etc/dhcpcd.conf les lignes suivantes, où X est à remplacer.  

    interface wlan0
    static ip_address=X.X.X.X/X
    static routers=X.X.X.X
    static domain_name_servers=X.X.X.X 8.8.8.8

Puis on redémarre.  

    reboot

On crée les dossiers qui contiendront le programme.  

    mkdir /prog
    chown nobody /prog
    mkdir /prog/code
    mkdir /prog/code/static
    mkdir /prog/code/templates
    mkdir /prog/log
    mkdir /prog/doc
    

Ce dossier sera partagé sur le réseau local via samba pendant la phase de test.  

    apt-get install samba

On ajoute au fichier /etc/samba/smb.conf les lignes suivantes.  

    [prog]
    path=/prog
    browsable=yes
    guest ok=yes
    read only=no

On redémarre.  

    reboot

En production, le partage ne sera utilisé que pour la maintenance du programme. Les lignes insérées précédemment dans le fichier smb.conf seront commentées et la propriété de /prog attribuée à root.  

    chown -R root /prog

On installe la librairie utilisée par le programme.  

    pip3 install flask

# Programmation

Le programme est constitué d’un script automate.py qui gère les actions de l’automate et d’un script interface.py qui fournit un serveur web sous Flask permettant d’afficher l’état de l’automate depuis un navigateur distant. Les deux scripts utilisent un fichier commun config.py qui contient les constantes et le format de la variable échangée entre les deux scripts.  
L’échange de données par socket

On crée une constante DATA qui sera copiée par chaque script dans une variable <data>.  
	
    # config.py
    
    """ This dict describes piface's states
        "echange": network's state
        "i0": 0, # switch on if hydraulic cylinder V1 is IN position
        "i1": 0, # switch on if hydraulic cylinder V1 is OUT position
        "i2": 0, # switch on if hydraulic cylinder V2 is IN position
        "i3": 0, # switch on if hydraulic cylinder V2 is OUT position
        "i4": 0, # goal of the electronic meter reached
        "i5": 0, # pushbutton to start the cycle
        "i6": 0, # switch On/Off cycle
        "i7": 0, # pushbutton emergency stop, mode NC
        "o0": 0, # to turn on V1
        "o1": 0, # to turn on V2
        "o2": 0, # to decrement the electronic meter
        "o3": 0, # to reset the electronic meter
        "o4": 0, # indicator light successful
        "o5": 0, # indicator light automate running
        "o6": 0, # indicator light automate initializing
        "o7": 0,  # indicator light automate emergency stopping
        "compteur" : 0 # number cycle
    """
 
    DATA = { "echange": False, "i0": 0, "i1": 0, "i2": 0, "i3": 0, "i4": 0, "i5": 0, "i6": 0, "i7": 0, "o0": 0, "o1": 0, "o2": 0, "o3": 0, "o4": 0, "o5": 0, "o6": 0, "o7": 0, "compteur" : 0}

	
    # interface.py
    # automate.py
    
    from config import *
    
    data = DATA.copy()

On crée le socket côté serveur.  

    # interface.py
    
    import socket
    
    """ Server network socket for <data> exchange """
    serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def connectServeur():
        """ This connects <client>
        """
        global serveur, client
        try:
            serveur.bind((IP, PORT))
            print ("Ready for connection")
            serveur.listen(1)
            client, ipClient = serveur.accept()
            print ("Client connected")
        except socket.error:
            print ("Network socket is down")

On crée le socket côté client.  
	
    # automate.py
    
    import socket
    
    """ Client network socket for <data> exchange """
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def connectClient():
        """ This connects <client>
        """
        global client
        try:
            client.connect((IP, PORT))
            print('Client connected')
        except socket.error:
            print ("Network socket is down")

Sur l’automate, on importe la librairie qui permet de gérer la carte et on crée un fonction qui met à jour sa variable <data> puis l’envoie sous forme d’un objet Json via le socket créé précédemment.  

    # automate.py
    
    import pifacedigitalio
    import json
    
    """ Reset interface's Piface """
    pifacedigitalio.init()
    pfd = pifacedigitalio.PiFaceDigital()
    
    def updateData():
        """ This updates <data> with piface's state and sends with <client>
            <data> is serializing with a json object before to send
        """
        global data, pfd, client
        data['i0'] = pfd.input_pins[0].value
        data['i1'] = pfd.input_pins[1].value
        data['i2'] = pfd.input_pins[2].value
        data['i3'] = pfd.input_pins[3].value
        data['i4'] = pfd.input_pins[4].value
        data['i5'] = pfd.input_pins[5].value
        data['i6'] = pfd.input_pins[6].value
        data['i7'] = pfd.input_pins[7].value  
        data['o0'] = pfd.output_pins[0].value
        data['o1'] = pfd.output_pins[1].value
        data['o2'] = pfd.output_pins[2].value
        data['o3'] = pfd.output_pins[3].value
        data['o4'] = pfd.output_pins[4].value
        data['o5'] = pfd.output_pins[5].value
        data['o6'] = pfd.output_pins[6].value
        data['o7'] = pfd.output_pins[7].value
        message = json.dumps(data)
        client.send(message.encode())

L’interface récupère les objets Json envoyés et met à jour sa variable <data> par une fonction exécutée en parallèle. L’accès concurrent à la variable <data> est géré par la variable <verrouData>.  
	
    # interface.py
    
    from threading import RLock, Thread
    import json
    import socket
    
    verrouData = RLock()
    
    def updateData():
        """ This receives and updates <data>
        """
        global client, data, verrouData
        while True:     
            message = client.recv(1024)
            with verrouData:
                data = json.loads(message.decode())
    
    if __name__ == '__main__':
        
        """ To run with parallel execution the function updateData """
        uds = Thread(target=updateData)
        uds.start()

L’automate est tout d’abord en attente de son initialisation.  
	
    # automate.py
    
    def getInputs():
        """ This returns a list that contains the states of inputs
        """
        global pfd
        inputs = list()
        for i in range(0,8):
            inputs.append(pfd.input_pins[i].value) 
        return inputs
    
    def init():
        """ This intialises
        """   
        global data, pfd
        print('Function init running')
        data['compteur'] = 0
        """ LED 6 is on """
        pfd.output_pins[6].value = 1
        pfd.output_pins[4].value = 0
        pfd.output_pins[7].value = 0
        pfd.output_pins[3].value = 1
        pfd.output_pins[0].value = 0
        pfd.output_pins[1].value = 0     
        updateData()
        while not(getInputs() == [1,0, 1, 0, 0, 0, 0, 1]):
            time.sleep(1)
        pfd.output_pins[6].value = 0
        pfd.output_pins[3].value = 0
        updateData()
    
    if __name__ == '__main__':
    
        print('Waiting for the function init')
        while not((pfd.input_pins[5].value == 1) and (pfd.input_pins[6].value == 0) and (pfd.input_pins[7].value == 1)):
            """ LED 6 flashes """
            pfd.output_pins[6].value = 1
            time.sleep(1)
            pfd.output_pins[6].value = 0
            time.sleep(1)
        init()

Ensuite, le cycle principal est lancé dans un thread en parallèle et contient la fonction start() et la fonction stop(). La fonction stop() permettra d’arrêter le cycle à tout moment.  

    # automate.py
    
    from threading import Thread
    import time
    
    class Action(Thread):
        """ Heritage of the Thread class
        """
        def __init__(self):
            Thread.__init__(self)
            self.suivant = True
    
        def run(self):
            """ This runs the cycle until the electronic meter is reached
            """
            global data, pfd
            print('Functiun cycle running')
            """ LED 5 is on """
            pfd.output_pins[5].value = 1
            data['compteur'] = data['compteur'] + 1
            updateData()
            while self.suivant and not(pfd.input_pins[4].value == 1):
                """ step 1 : V 1 go to OUT """
                while self.suivant and not(pfd.input_pins[1].value == 1):
                    pfd.output_pins[0].value = 1
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[0])
                """ step 2 : V 2 go to OUT """
                while self.suivant and not(pfd.input_pins[3].value == 1):
                    pfd.output_pins[1].value = 1
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[1])            
                """ step 3 : V 1 go to IN """
                while self.suivant and not(pfd.input_pins[0].value == 1):
                    pfd.output_pins[0].value = 0
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[2])
                """ step 4 : V 1 go to OUT """
                while self.suivant and not(pfd.input_pins[1].value == 1):
                    pfd.output_pins[0].value = 1
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[3])            
                """ step 5 : V 2 go to IN """
                while self.suivant and not(pfd.input_pins[2].value == 1):
                    pfd.output_pins[1].value = 0
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[4])
                """ step 6 : V 1 go to IN """
                while self.suivant and not(pfd.input_pins[0].value == 1):
                    pfd.output_pins[0].value = 0
                    updateData()
                updateData()
                if self.suivant : time.sleep(TEMPO[5])
                """ The electronic meter decrements"""
                if self.suivant : 
                    pfd.output_pins[2].value = 1 
                    time.sleep(1)
                    pfd.output_pins[2].value = 0
                    data['compteur'] = data['compteur'] + 1
                    updateData()
            """The cycle is finished """
            """ LED 4 is on """
            if self.suivant : 
                pfd.output_pins[4].value = 1
                updateData()
    
        def stop(self):
            """ Function to stop the thread
            """
            self.suivant = False
    
    """ Program """
    if __name__ == '__main__':
    
            print('Waiting for the function action')
            while not((pfd.input_pins[5].value == 1) and (pfd.input_pins[6].value == 1) and (pfd.input_pins[7].value == 1) and (pfd.input_pins[4].value == 0)):
                """ LED 5 flashes """
                pfd.output_pins[5].value = 1
                time.sleep(1)
                pfd.output_pins[5].value = 0
                time.sleep(1)
            action = Action()
            action.start()
    
            suivant = True
            while suivant:
                if (pfd.input_pins[7].value == 0):
                    print('Emergency stop')
                    action.stop()
                    """ LED 7 is on """
                    pfd.output_pins[7].value = 1
                    suivant = False
                if (pfd.input_pins[6].value == 0):
                    print('Cycle stop')
                    action.stop()
                    suivant = False
                if (pfd.output_pins[4].value == 1):
                    print('Cycle finished')
                    suivant = False
            """ LED 5 is off """
            pfd.output_pins[5].value = 0
            updateData()

L’interface se présente sous la forme d’un serveur web Flask et de deux pages login.html et accueil.htm sous templates. Pour la présentation des pages html, on utilise la librairie jQuery Mobile qui est stockée localement sous static.  
	
    # interface.py
    
    from flask import Flask, render_template, redirect, url_for, request, session
    
    import os
    import re
    
    """ Web server flask """
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.urandom(24)
    
    @app.route('/', methods=['GET', 'POST'])
    def login():
        if (request.method == 'GET'):
            return render_template('login.html', ip=IP)                    
        if request.method == 'POST':
            """ To get the sended user """
            user = request.form['user']
            """ To clean the string """
            user = re.sub(r'\W', '', user)
            if (user in PASS):
                """ The user is known"""
                session['user'] = user
                session.modified = True
                return redirect(url_for('accueil'))
            return redirect(url_for('login'))
    
    @app.route('/logout')
    def logout():
        session['user'] = None
        session.modified = True   
        return redirect(url_for('login'))
    
    @app.route('/accueil', methods=['GET', 'POST'])
    def accueil():
        global data
        """ To control access """
        if (session['user'] in PASS):
            if (request.method == 'GET'):
                with verrouData:
                    return render_template('accueil.html', ip=IP, data=data)
        else:
            return render_template('login.html', ip=IP)
    
    """ Programm """
    if __name__ == '__main__':
    
        app.run(host='0.0.0.0', debug=False)

    !DOCTYPE html>
    <html>
    
    <head>
        <title>MyRobot</title> 
        <meta name=viewport content="width=device-width, user-scalable=no">
        <link rel=icon type=image/png href={{ url_for('static', filename='favicon.ico') }}>
        <link rel=apple-touch-icon href={{ url_for('static', filename='appleicon.png') }} />
        <link rel=stylesheet href={{ url_for('static', filename='jquery.mobile-1.4.5.min.css') }}/>
        <link rel=stylesheet href={{ url_for('static', filename='style.css') }}/>
        <script src={{ url_for('static', filename='jquery-1.11.3.min.js') }}></script>
        <script src={{ url_for('static', filename='jquery.mobile-1.4.5.min.js') }}></script>
        <script src={{ url_for('static', filename='jquery.autoheight.js') }}></script>
    </head>
    
    
    <body>
    <div data-role="page" id="MyRobot">
    
        <!--header-->
        <div data-role=header>
            <h1>Interface du robot</h1>
            {% set adresse = ['http://', ip, ':5000/logout']|join %}
            <a href="{{adresse}}" data-icon="delete" class="ui-btn-right">Se déconnecter</a>
        </div>
        
        <!--content-->
        <div data-role="content">
            
        <table>
        <tbody>
        <tr>
            <td>
            <ul data-role="listview" data-inset=true data-filter=false>
                <li data-role="list-divider"><div class=centrer>ENTREES</div></li>
                {% for i in ['i0', 'i1', 'i2', 'i3', 'i4', 'i5', 'i6', 'i7'] %}
                    <li><div class=centrer>
                    {% if i == 'i0' %}
                        {% set name = "V1 IN" %}
                    {% elif i == 'i1' %}
                        {% set name = "V1 OUT" %}
                    {% elif i == 'i2' %}
                        {% set name = "V2 IN" %}    
                    {% elif i == 'i3' %}
                        {% set name = "V2 OUT" %}
                    {% elif i == 'i4' %}
                        {% set name = "Electronic meter reached" %} 
                    {% elif i == 'i5' %}
                        {% set name = "Start" %}    
                    {% elif i == 'i6' %}
                        {% set name = "Cycle" %}    
                    {% elif i == 'i7' %}
                        {% set name = "Emergency stop" %}
                    {% endif %}
                    <p class=centrer>{{name}}</p>           
                    <select data-role=slider disabled="disabled">                     
                    {% set selected = '' %}                                 
                    {% if data[i] == 1 %}
                        {% set selected="selected" %}                                           
                    {% endif %}
                        <option >off</option>
                        <option {{selected}}>on</option>
                    </select>
                    </div></li>
                {%endfor%}
            </ul> 
            </td>
    
            <td>
            <ul data-role="listview" data-inset=true data-filter=false>
                <li data-role="list-divider"><div class=centrer>SORTIES</div></li>
                {% for i in ['o0', 'o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7'] %}
                    <li><div class=centrer>
                    {% if i == 'o0' %}
                        {% set name = "Action V1" %}
                    {% elif i == 'o1' %}
                        {% set name = "Action V2" %}
                    {% elif i == 'o2' %}
                        {% set name = "To decrement e meter" %} 
                    {% elif i == 'o3' %}
                        {% set name = "To reset e meter" %}
                    {% elif i == 'o4' %}
                        {% set name = "Cycle finished" %}   
                    {% elif i == 'o5' %}
                        {% set name = "Cycle running" %}    
                    {% elif i == 'o6' %}
                        {% set name = "Cycle init" %}   
                    {% elif i == 'o7' %}
                        {% set name = "Emergency stop" %}
                    {% endif %}
                <p class=centrer>{{name}}</p>
                    <select data-role=slider disabled="disabled">                     
                    {% set selected = '' %}                                 
                    {% if data[i] == 1 %}
                        {% set selected="selected" %}                                           
                    {% endif %}
                        <option >off</option>
                        <option {{selected}}>on</option>
                    </select>
                    </div></li>
                {%endfor%}
            </ul>         
            </td>
    
            <td>
            <ul data-role="listview" data-inset=true data-filter=false>
                <li data-role="list-divider"><div class=centrer>CONTROLES</div></li>
                {% for i in ['echange'] %}
                    <li><div class=centrer>
                    <p class=centrer>Network Socket</p>
                    <select data-role=slider disabled="disabled">                     
                    {% set selected = '' %}                                 
                    {% if data[i] == 1 %}
                        {% set selected="selected" %}                                           
                    {% endif %}
                        <option >down</option>
                        <option {{selected}}>up</option>
                    </select>
                    </div></li>
                {%endfor%}
                <li><div class=centrer>
                    <p class=centrer>Number Cycle</p>{{data['compteur']}}
                </div></li>
            </ul>
            </td>
        </tr>
        </tbody>          
        </table>
    
        <!--footer-->
        <div data-role="footer">
            <h4>MyRobot - franckSalle.fr</h4>
        </div>        
    </div>    
    </body>
    
    </html>
    
    {% set adresse = ['http://', ip, ':5000/accueil']|join %}
    <script>
        function refreshMe() {
            window.location.replace("{{adresse}}");   
        }
        setInterval(refreshMe, 10000);
    </script>

    <!DOCTYPE html>
    <html>
    
    <head>
        <title>MyRobot</title> 
        <meta name=viewport content="width=device-width, user-scalable=no">
        <link rel=icon type=image/png href={{ url_for('static', filename='favicon.ico') }}>
        <link rel=apple-touch-icon href={{ url_for('static', filename='appleicon.png') }} />
        <link rel=stylesheet href={{ url_for('static', filename='jquery.mobile-1.4.5.min.css') }}/>
        <link rel=stylesheet href={{ url_for('static', filename='style.css') }}/>
        <script src={{ url_for('static', filename='jquery-1.11.3.min.js') }}></script>
        <script src={{ url_for('static', filename='jquery.mobile-1.4.5.min.js') }}></script>
        <script src={{ url_for('static', filename='jquery.autoheight.js') }}></script>
        <script> $( document ).ready(function() {$( "#login" ).focus();}); </script>
    </head>
    
    <body>
        <div data-role=dialog data-title="MyRobot">
            <!--header-->
            <div data-role=header>
                <h1>Interface du robot</h1>
            </div>
            <!--content-->
            <div data-role=content>
                <div class="centrer">
                    <img  src={{ url_for('static', filename='logo.png') }}>
                    {% set adresse = ['http://', ip, ':5000']|join %}
                    <form action="{{adresse}}" method=post>
                        <input id=login name=user type=password required>                     
                    </form>
                </div>
            </div>
            <!--footer-->
            <div data-role=footer>
                <h4>MyRobot - franckSalle.fr</h4>
            </div>        
    </body>